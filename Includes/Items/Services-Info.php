<section class="services">

<div class="container">
  <div class="row">
    <div class="col-md-12 services-all">

      <?php if ( have_posts() ) : ?>
        <?php query_posts('cat=4 '); ?>
      <?php while ( have_posts() ) : the_post(); ?>
        <div id="post-<?php the_ID(); ?> "<?php post_class(); ?>>

             <div class="col-md-12 services-all-item">
               <h1><?php the_title(); ?></h1>

             <?php the_content(); ?>
             <?php edit_post_link(); ?>
             </div>

        </div><!--end post header-->


      <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

      <?php else : ?>
      <?php endif; ?>


      </div>
    </div>
    </div>


</section>
