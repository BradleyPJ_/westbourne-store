<section class="service-hide">

  <div class="container">
    <div class="row">
      <div class="col-md-12 services-message">
        <a href=""><h1>Services</h1></a>
        <hr>
      </div>
    </div>

  <div class="row">

    <?php if ( have_posts() ) : ?>
      <?php query_posts('cat=5 '); ?>
    <?php while ( have_posts() ) : the_post(); ?>
      <div id="post-<?php the_ID(); ?> " <?php post_class(); ?>>


           <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
           <?php the_content(); ?>
           <?php edit_post_link(); ?>



        </div><!--end post-->
    <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
    <?php else : ?>
    <?php endif; ?>

    <?php // TODO: find way to make this work. upto 5 images next to each other. in wordpress and that is easy to edit. ?>

  </div>
  <hr class="breaker">
</div>

</section>
