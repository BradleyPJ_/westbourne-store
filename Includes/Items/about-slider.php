<section class="about">

  <div class="container">
    <div class="row">
      <div class="col-md-12 about-message">

		<?php if ( have_posts() ) : ?>
  <?php query_posts('cat=8 '); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

       <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
       <?php the_content(); ?>

    </div><!--end post header-->

    <!--end post-->
<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
<?php else : ?>
<?php endif; ?>

      </div>
    </div>
</div>

</section>
