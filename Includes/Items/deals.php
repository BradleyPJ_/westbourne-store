<section class="deals">

  <div class="container">
    <div class="row">
      <div class="col-md-12 deals-message">
        <h1>Hot Deals</h1>
        <hr>
      </div>
    </div>
    <div class="row">

<?php if ( have_posts() ) : ?>
  <?php query_posts('cat=2 '); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <div id="post-<?php the_ID(); ?> col-md-3" <?php post_class(); ?>>
    <div class="col-md-3">

       <h2><?php the_title(); ?></h2>
       <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
       <?php the_content(); ?>
       <?php edit_post_link(); ?>

    </div><!--end post header-->


    </div><!--end post-->
<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
    <div class="navigation index">
       <div class="alignleft"><?php next_posts_link( 'Older Entries' ); ?></div>
       <div class="alignright"><?php previous_posts_link( 'Newer Entries' ); ?></div>
    </div><!--end navigation-->
<?php else : ?>
<?php endif; ?>

</div>
<hr class="breaker">
</div>

</section>
