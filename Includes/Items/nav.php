<section class ="quickContact">
  <div class="container">
    <div class="row">
      <div class="col-lg-2 col-lg-offset-2">
        <p>Phone: <?php echo get_option('Phone'); ?> </p>
      </div>
      <div class="col-lg-3 col-lg-offset-3">
        <p>Email: <?php echo get_option('Email'); ?></p>
      </div>
    </div>
  </div>
</section>

<nav class="navbar navbar-default">
<div class="container">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand col-md-12" href="#">
      <img class = "img-responsive "alt="Brand" src="<?php echo get_template_directory_uri(); ?>/Images/logo.png">
    </a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav navbar-right">
      <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
    </ul>
  </div><!-- /.navbar-collapse -->
</div>
</nav>
