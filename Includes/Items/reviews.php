
  <section class="testimonials">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Reviews</h1>
          <hr class="breaker">

<?php query_posts('cat=3'); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>
  <div id="post-<?php the_ID(); ?> " class ="col-md-6 testimonials-feedback" <?php post_class(); ?>>
    <div>

       <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
       <?php the_content(); ?>
       <?php edit_post_link(); ?>

    </div><!--end post header-->


    </div><!--end post-->
<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
<?php else : ?>
<?php endif; ?>


</div>
</div>
<hr class="breaker">
</div>
</section>
