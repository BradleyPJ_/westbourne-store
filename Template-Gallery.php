<?php get_header(); ?>

<?php
/**
 * Template Name: Village-store-Gallery
 *
 * @package WordPress
 * @subpackage Village-store
 * @since Village-store
 */

 ?>

    <section>

        <div class="container">
          <div class="row">
            <div class="col-md-12">
            <?php if ( have_posts() ) : ?>
              <?php query_posts('cat=7 '); ?>
            <?php while ( have_posts() ) : the_post(); ?>
              <div id="post-<?php the_ID(); ?> "  <?php post_class(); ?>>


                    <h1><?php the_title(); ?></h1>
                   <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
                   <?php the_content(); ?>
                   <?php edit_post_link(); ?>
                </div><!--end post header-->

            <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

            <?php else : ?>
            <?php endif; ?>
            </div>
      </div>
      </div>
    </section>



  <?php get_footer(); ?>
