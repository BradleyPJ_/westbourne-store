<section class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-2 footer-list footer-hide">
          <ul class="sitemap">
            <li class = "title">Sitemap</li>
            <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
          </ul>
        </div>

            <div class = "footer-hide col-md-2 footer-list">
              <li class = "title">Services</li>
              <?php if ( have_posts() ) : ?>
                <?php query_posts('cat=4 '); ?>
              <?php while ( have_posts() ) : the_post(); ?>
                <div id="post-<?php the_ID(); ?> "<?php post_class(); ?>>

                  <li><a href="#"><?php the_title(); ?></a></li>

                </div><!--end post header-->

                <!--end post-->
              <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

              <?php else : ?>
              <?php endif; ?>

              </div>



        <div class="col-md-4 col-md-offset-4 footer-list">
          <li class = "title">Contact</li>
          <li class = "contact">Phone: <?php echo get_option('Phone'); ?></li>
          <li class = "contact">Email: <?php echo get_option('Email'); ?></li>

        </div>

      </div>
    </div>
  </div>

</section>

</body>
</html>
