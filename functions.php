<?php


// TODO: look for for a better way to do a settings page / customising page

function theme_settings_page(){
  ?>
    <div class="wrap">
    <h1>Theme Panel</h1>
    <form method="post" action="options.php">
        <?php
            settings_fields("section");
            do_settings_sections("theme-options");
            submit_button();
        ?>
    </form>
  </div>
<?php

}

function display_Phone_element()
{
	?>
    	<input type="text" name="Phone" id="Phone" value="<?php echo get_option('Phone'); ?>" />
    <?php
}

function display_Email_element()
{
	?>
    	<input type="text" name="Email" id="Email" value="<?php echo get_option('Email'); ?>" />
    <?php
}

function display_Monday_OpenHours_element()
{
	?>
    	<input type="text" name="Monday" id="Monday" value="<?php echo get_option('Monday'); ?>" />
    <?php
}

function display_Tuesday_OpenHours_element()
{
	?>
    	<input type="text" name="Tuesday" id="Tuesday" value="<?php echo get_option('Tuesday'); ?>" />
    <?php
}

function display_Wednesday_OpenHours_element()
{
	?>
    	<input type="text" name="Wednesday" id="Wednesday" value="<?php echo get_option('Wednesday'); ?>" />
    <?php
}

function display_Thursday_OpenHours_element()
{
	?>
    	<input type="text" name="Thursday" id="Thursday" value="<?php echo get_option('Thursday'); ?>" />
    <?php
}

function display_Friday_OpenHours_element()
{
	?>
    	<input type="text" name="Friday" id="Friday" value="<?php echo get_option('Friday'); ?>" />
    <?php
}

function display_Saturday_OpenHours_element()
{
	?>
    	<input type="text" name="Saturday" id="Saturday" value="<?php echo get_option('Saturday'); ?>" />
    <?php
}

function display_Sunday_OpenHours_element()
{
	?>
    	<input type="text" name="Sunday" id="Sunday" value="<?php echo get_option('Sunday'); ?>" />
    <?php
}

function display_Location_element()
{
	?>
    	<input type="text" name="Location" id="Location" value="<?php echo get_option('Location'); ?>" />
    <?php
}

function display_LocationMap_element()
{
	?>
    	<input type="text" name="LocationMap" id="LocationMap" value="<?php echo get_option('LocationMap'); ?>" />
    <?php
}


function display_theme_panel_fields()
{
	add_settings_section("section", "All Settings", null, "theme-options");


  //Contact Details
	add_settings_field("Phone", "Phone Number", "display_Phone_element", "theme-options", "section");
  add_settings_field("Email", "Email Address", "display_Email_element", "theme-options", "section");

  //Open Hours
  add_settings_field("Monday", "Monday Hours", "display_Monday_OpenHours_element", "theme-options", "section");
  add_settings_field("Tuesday", "Tuesday Hours", "display_Tuesday_OpenHours_element", "theme-options", "section");
  add_settings_field("Wednesday", "Wednesday Hours", "display_Wednesday_OpenHours_element", "theme-options", "section");
  add_settings_field("Thursday", "Thursday Hours", "display_Thursday_OpenHours_element", "theme-options", "section");
  add_settings_field("Friday", "Friday Hours", "display_Friday_OpenHours_element", "theme-options", "section");
  add_settings_field("Saturday", "Saturday Hours", "display_Saturday_OpenHours_element", "theme-options", "section");
  add_settings_field("Sunday", "Sunday Hours", "display_Sunday_OpenHours_element", "theme-options", "section");

  //Location and Map
  add_settings_field("Location", "Location Address", "display_Location_element", "theme-options", "section");
  add_settings_field("LocationMap", "Location Map IFrame SRC", "display_LocationMap_element", "theme-options", "section");

  //Contact Details
  register_setting("section", "Phone");
  register_setting("section", "Email");

  //Open Hours
  register_setting("section", "Monday");
  register_setting("section", "Tuesday");
  register_setting("section", "Wednesday");
  register_setting("section", "Thursday");
  register_setting("section", "Friday");
  register_setting("section", "Saturday");
  register_setting("section", "Sunday");

  //Location and Map
  register_setting("section", "Location");
  register_setting("section", "LocationMap");


}
add_action("admin_init", "display_theme_panel_fields");

function add_theme_menu_item()
{
	add_menu_page("Theme Panel", "Theme Panel", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");




?>
