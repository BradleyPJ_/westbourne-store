<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Westbourne VillageStore</title>
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/Global.css" />
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

  <script src = "<?php echo get_template_directory_uri(); ?>/js/jquery-2.2.1.min.js"> </script>
  <script src = "<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"> </script>
  <script src = "<?php echo get_template_directory_uri(); ?>/js/Main.js"> </script>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta charset="UTF-8">
  <meta name="description" content="Aspire Roofing, Offering quality roofing services">
  <meta name="keywords" content="AspireRoofing, Roofing, Services">

</head>
<body>

<?php include("Includes/Items" . "/nav.php"); ?>
