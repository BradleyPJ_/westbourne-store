<?php get_header(); ?>


<?php include("Includes/Items" . "/about-slider.php"); ?>

<?php include("Includes/Items" . "/Services.php"); ?>

<?php include("Includes/Items" . "/deals.php"); ?>

<?php include("Includes/Items" . "/reviews.php"); ?>

<section class="Open-Hours">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Open Hours</h2>
                <h3>Monday: <?php echo get_option('Monday'); ?></h3>
                <h3>Tuesday: <?php echo get_option('Tuesday'); ?></h3>
                <h3>Wednesday: <?php echo get_option('Wednesday'); ?></h3>
                <h3>Thurday: <?php echo get_option('Thursday'); ?></h3>
                <h3>Friday: <?php echo get_option('Friday'); ?></h3>
                <h3>Saturday: <?php echo get_option('Saturday'); ?></h3>
                <h3>Sunday: <?php echo get_option('Sunday'); ?></h3>
            </div>
        </div>
        <hr class="breaker">
    </div>
</section>

<section class="Location">

    <div class="container">
        <div class="row">
            <div class="col-md-12 Location-message">
                <h1>Location</h1>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 Location-message">
                <a href="httds://goo.gl/maps/PF686XY2nst"><h3><?php echo get_option('Location'); ?></h3></a>
                <iframe src="<?php echo get_option('LocationMap'); ?>" + " width='600' height='450' frameborder='0' style='border:0' allowfullscreen"></iframe>
            </div>
        </div>
    </div>

</section>

<hr>


<?php get_footer(); ?>
